The plugin named "AdvancedDebugInfo (ADI)" is a modified version of the "DebugPanoramaDirectory" called plugin, it has been tested with V2.3.2 and V2.5 of PTP.

Plugin description:
Like the original plugin, ADI adds tour information as a comment into the tour skin file "XXX_skin.xml". The data comes as true CSV format so the info can easily be extracted for analysis or imported into a spreadsheet application.

All data lines start with a "debug data tag" which can be set via configuration menu, as well as the data separation character (or string). The second field of every data line specifies the section or topic, for example: general info, group info, pano info.

Things that can get reported:
The number of groups and panos.
Is project name and author info available?
Does the project (group, pano) have a description? (The description itself can also be included into the resulting output).
Which output formats were generated?
A list of groups with all embedded panos.
Is every pano added to an explicit group?
Does every group have a mapfile attached?
Is every pano linkedto at least once?
Has every pano at least one link to another pano?

Here is some data of a sample run:
<!--	Plugin: AdvancedDebugInfo
	Author: Michael Brüning <M.Bruening@compunics.com.ni
	Tested with: PTP V2.3.2 and V2.5
-->

<!--
# PROJECT INFO:
ADI,Project,NumberOfPanos,6
ADI,Project,NumberOfGroups,3
ADI,Project,TourTypes,Flash,HTML5,Desktop
ADI,Project,ProjectName,AutoQS
ADI,Project,ProjectAuthorName,m.bruening@compunics.com.ni
ADI,Project,ProjectAuthorWeb,http://360.compunics.com.ni


# GROUP INFO: TAG,GROUPTYPE,GROUP,ID,DIRECTORY,NUMBER-OF-PANOS,NUMBER-OF-MAPSPOTS,MAPFILE,DESCRIPTION?,GROUP-PANOS
ADI,PanoGroup,Explicit,Erdgeschoss,panogroup19,erdgeschoss_19,3,3,grundriss-innen_panogroup19.png,HasDescription,Wohnen,Flur,Bad
ADI,PanoGroup,Explicit,Aussen,panogroup12,aussen_12,2,2,grundriss-aussen_panogroup12.png,HasDescription,Eingang,Terrasse 1
ADI,PanoGroup,Implicit,Gruppe 217,panogroup217,gruppe_217_217,1,0,,NoDescription,Hausansicht West

# PANORAMA LIST: TAG,NUMBER-OF-PANOS,PANOS
ADI,PanoList,6,Wohnen,Flur,Bad,Eingang,Terrasse 1,Hausansicht West

# PANORAMA INFO: TAG,PANONAME ,ID ,DIRECTORY
ADI,PanoInfo,Wohnen,pano17,sala_17,NoDescription
ADI,PanoInfo,Flur,pano16,oficinaprivada_16,NoDescription
ADI,PanoInfo,Bad,pano172,bad_172,NoDescription
ADI,PanoInfo,Eingang,pano10,afuera_10,HasDescription
ADI,PanoInfo,Terrasse 1,pano218,terrasse_1_218,NoDescription
ADI,PanoInfo,Hausansicht West,pano216,hausansicht_west_216,HasDescription

# PANORAMA LINKSTO INFO: TAG,PANONAME
ADI,PanoLinksTo,Wohnen,Flur
ADI,PanoLinksTo,Flur,Wohnen,Eingang,Bad
ADI,PanoLinksTo,Bad,Flur
ADI,PanoLinksTo,Eingang,Wohnen
ADI,PanoLinksTo,Terrasse 1,Hausansicht West
ADI,PanoLinksTo,Hausansicht West

# PANORAMA LINKSTO ERRORS: TAG,PANONAME
ADI,PanoLinksToError,Hausansicht West

# PANORAMA LINKEDBY INFO: TAG,PANONAME,LINKING PANOS
ADI,PanoLinkedBy,Wohnen,Flur,Eingang
ADI,PanoLinkedBy,Flur,Wohnen,Bad
ADI,PanoLinkedBy,Bad,Flur
ADI,PanoLinkedBy,Eingang,Flur
ADI,PanoLinkedBy,Terrasse 1
ADI,PanoLinkedBy,Hausansicht West,Terrasse 1

# PANORAMA LINKEDBY ERRORS: TAG,PANONAME
ADI,PanoLinkedByError,Terrasse 1
-->



